package ull.patrones.figuras;

/**
 * Clase encargada de hacer los c�lculos de un c�rculo
 * @author Orlandy Ariel S�nchez A.
 */
public class Circulo implements Figura
{
	// ATRIBUTOS
	private Double mRadio;
	private Double mArea;

	// CONSTRUCTOR/ES & M�TODOS
	public Circulo(Double aRadio)
	{
		this.mRadio = aRadio;
		this.mArea = 0D;
	}

	public Double getMRadio()
	{
		return mRadio;
	}

	public void setMRadio(Double aRadio)
	{
		this.mRadio = aRadio;
	}

	@Override
	public Double fArea()
	{
		this.mArea = Math.PI * Math.pow(mRadio, 2.0);
		return this.mArea;
	}

	@Override
	public Double fVolumen()
	{
		return ((double) 4 / 3 * Math.PI) * (Math.pow(mRadio, 3.0));
	}

	@Override
	public Double fPerimetro()
	{// 2pi*radio
		return 2 * Math.PI * mRadio;
	}

}
