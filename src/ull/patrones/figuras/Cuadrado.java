package ull.patrones.figuras;

/**
 * Clase encargada de hacer los c�lculos de un cuadrado
 * @author Orlandy Ariel S�nchez A.
 */
public class Cuadrado implements Figura
{
	// ATRIBUTOS
	private Double mLado;

	// CONSTRUCTOR/ES & M�TODOS
	public Cuadrado(Double aLado)
	{
		this.mLado = aLado;
	}

	@Override
	public Double fArea()
	{
		return Math.pow(mLado, 2);
	}

	@Override
	public Double fVolumen()
	{
		return Math.pow(this.mLado, 3);
	}

	@Override
	public Double fPerimetro()
	{// suma de los 4 lados
		return 4 * mLado;
	}
}
