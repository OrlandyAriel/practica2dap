package ull.patrones.figuras;

/**
 * Clase encargada de hacer los c�lculos de un Tri�ngulo,
 * asumo que el tri�ngulo es equilatero
 * @author Orlandy Ariel S�nchez A.
 */
public class Triangulo implements Figura
{
	// ATRIBUTOS
	private Double mBase;
	private Double mAltura;

	// CONSTRUCTOR & M�TODOS
	/**
	 * 
	 * @param aBase
	 * @param aAltura
	 */
	public Triangulo(Double aBase, Double aAltura)
	{
		this.mBase = aBase;
		this.mAltura = aAltura;
	}

	@Override
	public Double fArea()
	{
		return (this.mBase * this.mAltura) / 2.0;
	}

	@Override
	public Double fVolumen()
	{
		return ((mBase * mBase) * mAltura) / 3.0;
	}

	@Override
	public Double fPerimetro()
	{// 3*base, tri�ngulo equilatero
		return 3 * mBase;
	}
}
