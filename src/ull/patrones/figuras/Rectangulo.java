package ull.patrones.figuras;
/**
 * Clase encargada de hacer los c�lculos de un rect�ngulo
 * @author Orlandy Ariel S�nchez A.
 *
 */
public class Rectangulo implements Figura
{
	//ATRIBUTOS
	private Double mLongitud;
	private Double mAltura;
	private Double mProfundidad;
	//CONSTRUCTOR Y M�TODOS
	public Rectangulo(Double aLongitud, Double aAltura, Double aProdundidad)
	{
		this.mLongitud = aLongitud;
		this.mAltura = aAltura;
		this.mProfundidad = aProdundidad;
	}

	@Override
	public Double fArea()
	{
		return mLongitud * mAltura;
	}

	@Override
	public Double fVolumen()
	{
		// v = longitud*profundidad*altura
		return mLongitud * mProfundidad * mAltura;
	}

	@Override
	public Double fPerimetro()
	{
		return 2 * mLongitud + 2 * mAltura;
	}

}
