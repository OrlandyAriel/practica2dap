package ull.patrones.figuras;
/**
 * Clase encargada de hacer los c�lculos de un trapecio,
 * para ello asumo que el trapecio es is�sceles.
 * @author Orlandy Ariel S�nchez A.
 *
 */
public class Trapecio implements Figura
{
	private Double mBaseMenor;
	private Double mBaseMayor;
	private Double mLado;
	private Double mAltura;
	/**
	 * Constructor
	 * @param aBaseMenor
	 * @param aBaseMayor
	 * @param aAltura
	 * @param aLado
	 */
	public Trapecio(Double aBaseMenor, Double aBaseMayor, Double aAltura, Double aLado)
	{
		this.mBaseMenor = aBaseMenor;
		this.mBaseMayor = aBaseMayor;
		this.mAltura = aAltura;
		this.mLado = aLado;

	}

	@Override
	public Double fArea()
	{
		return (mBaseMayor + mBaseMenor) / 2.0 * mAltura;
	}

	@Override
	public Double fVolumen()
	{
		Double tAreaBmayor = mBaseMayor * mBaseMayor;
		Double tAreaBmenor = mBaseMenor * mBaseMenor;
		Double tRaizAreas=Math.sqrt(tAreaBmayor *tAreaBmenor);
		Double tResult = (mAltura/3.0)*(tAreaBmayor+tAreaBmenor+tRaizAreas);
		return tResult;
	}

	@Override
	public Double fPerimetro()
	{
		return mBaseMayor + mBaseMenor + 2 * mLado;
	}

}
