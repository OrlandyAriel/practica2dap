package ull.patrones.figuras;
/**
 * Clase encargada de hacer los c�lculos de un Hexagono
 * @author Orlandy Ariel S�nchez A.
 *
 */
public class Hexagono implements Figura
{	//ATRIBUTOS
	private Double mApotema;
	private Double mAltura;
	private Double mPerimetro;
	private Double mLongitudLado;
	//CONSTRUCTOR Y M�TODOS
	/**
	 * 
	 * @param lado
	 * @param aAltura
	 */
	public Hexagono(Double lado, Double aAltura)
	{
		this.mLongitudLado = lado;
		this.mAltura = aAltura;
		fApotema();
		fPerimetro();
	}
	/**
	 * Fuci�n privada para el c�lculo de la apotema
	 */
	private void fApotema()
	{
		mApotema = Math.sqrt((Math.pow(mLongitudLado, 2) - Math.pow((mLongitudLado / 2.0), 2)));
	}

	@Override
	public Double fArea()
	{
		fPerimetro();
		return (mPerimetro * mApotema) / 2;
	}

	@Override
	public Double fVolumen()
	{// volumen del prisma hexagonal regular v= 3 * L * ap * H
		return 3.0 * mLongitudLado * mApotema * mAltura;
	}

	@Override
	public Double fPerimetro()
	{
		mPerimetro = 6 * mLongitudLado;
		return mPerimetro;
	}
}
