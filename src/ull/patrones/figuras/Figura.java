package ull.patrones.figuras;

/**
 *	Interfaz con los distintos c�lculos que se pueden realizar
 * @author Orlandy Ariel S�nchez A.
 */
public interface Figura
{
	public Double fArea();

	public Double fVolumen();

	public Double fPerimetro();
}
