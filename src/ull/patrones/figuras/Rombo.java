package ull.patrones.figuras;
/**
 * Clase encargada de hacer los c�lculos de un rombo
 * @author Orlandy Ariel S�nchez A.
 *
 */
public class Rombo implements Figura
{	//ATRIBUTOS
	private Double mDiagonalMayor;
	private Double mDiagonalMenor;
	private Double mLado;
	//CONSTRUCTOR Y M�TODOS
	/**
	 * 
	 * @param aDiagMayor
	 * @param aDiagoMenor
	 * @param aLado
	 */
	public Rombo(Double aDiagMayor, Double aDiagoMenor, Double aLado)
	{
		this.mDiagonalMayor = aDiagMayor;
		this.mDiagonalMenor = aDiagoMenor;
		this.mLado = aLado;
	}

	@Override
	public Double fArea()
	{
		return (mDiagonalMayor * mDiagonalMenor) / 2.0;
	}

	@Override
	public Double fVolumen()
	{
		Double tRaizDeDos = Math.sqrt(2.0);
		Double tLadoAlCubo = mLado * mLado * mLado;
		Double tVol = (1.0 / 3.0) * tRaizDeDos * tLadoAlCubo;
		return tVol;
	}

	@Override
	public Double fPerimetro()
	{
		return 4 * mLado;
	}

}
