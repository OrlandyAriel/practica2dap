package ull.patrones.enumeradores;
/**
 * Enumerador para los distintos tipos de figuras
 * @author Orlandy Ariel S�nchez A.
 *
 */
public enum TipoFigura
{
	CUADRADO,
	HEXAGONO,
	RECTANGULO,
	ROMBO,
	TRAPECIO,
	TRIANGULO,
	CIRCULO
}
