package ull.patrones.ui.figuras;


import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Polygon;
import java.awt.BasicStroke;
import java.awt.Color;
import javax.swing.JPanel;

import ull.patrones.enumeradores.TipoFigura;
/**
 * Clase que representa los distintos tipos de pol�gonos en ventana
 * @author Orlandy Ariel S�nchez A.
 *
 */
public class PanelPoligono extends JPanel
{
	private Polygon mPoligono;
	private int mCoorX[];
	private int mCoorY[];
	private TipoFigura mPoligonoSelec;

	public PanelPoligono(TipoFigura aPoligono)
	{
		mPoligonoSelec = aPoligono;
		initCoordenadas();
		setVisible(true);
	}
	/**
	 * M�todo para as�gnar las coordenadas de pol�gono elegido
	 */
	private void initCoordenadas()
	{
		switch (mPoligonoSelec)
		{
		case HEXAGONO:
			mCoorX = new int[] { 176, 220, 243, 220, 176, 150 };
			mCoorY = new int[] { 40, 40, 82, 128, 128, 82 };
			mPoligono = new Polygon(mCoorX, mCoorY, 6);
			break;
		case ROMBO:
			mCoorX = new int[] { 200, 280, 200, 120 };
			mCoorY = new int[] { 10, 90, 170, 90 };
			mPoligono = new Polygon(mCoorX, mCoorY, 4);
			break;
		case TRAPECIO:
			mCoorX = new int[] { 160, 240, 280, 120 };
			mCoorY = new int[] { 50, 50, 150, 150 };
			mPoligono = new Polygon(mCoorX, mCoorY, 4);
			break;
		case TRIANGULO:
			mCoorX = new int[] { 200, 280, 120 };
			mCoorY = new int[] { 10, 170, 170 };
			mPoligono = new Polygon(mCoorX, mCoorY, 3);
			break;
		case CUADRADO:
			mCoorX = new int[] { 120, 280, 280, 120 };
			mCoorY = new int[] { 10, 10, 170, 170 };
			mPoligono = new Polygon(mCoorX, mCoorY, 4);
			break;
		case RECTANGULO:
			mCoorX = new int[] { 120, 280, 280, 120 };
			mCoorY = new int[] { 50, 50, 150, 150 };
			mPoligono = new Polygon(mCoorX, mCoorY, 4);
			break;
		default:
			break;
		}
	}

	@Override
	public void paint(Graphics g)
	{
		super.paint(g);
		Graphics2D g2 = (Graphics2D) g;
		g2.setStroke(new BasicStroke(2.0f));
		g2.setColor(Color.WHITE);// color del fondo de la figura
		g2.fill(mPoligono);
		g2.setColor(Color.GRAY);// color de las lineas de la figura
		g2.draw(mPoligono);
	}
}