package ull.patrones.ui.figuras;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.geom.Ellipse2D;

import javax.swing.JPanel;
/**
 * Clase encargada de dibujar un c�rculo en la ventana
 * @author Orlandy Ariel S�nchez A.
 *
 */
public class PanelCirculo extends JPanel
{
	private Ellipse2D mCirculo;

	public PanelCirculo()
	{
		setVisible(true);
	}

	@Override
	public void paint(Graphics g)
	{
		super.paint(g);
		Graphics2D g2 = (Graphics2D) g;

		g2.setStroke(new BasicStroke(2.0F));
		mCirculo = new Ellipse2D.Float(120F, 10F, 150F, 150F);
		g2.setColor(Color.WHITE); // relleno del circulo
		g2.fill(mCirculo);
		g2.setColor(Color.GRAY);
		g2.draw(mCirculo);
	}
}
