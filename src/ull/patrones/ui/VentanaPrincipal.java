package ull.patrones.ui;

import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.ScrollPane;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.text.DecimalFormat;

import javax.swing.ButtonGroup;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JRadioButton;

import ull.patrones.enumeradores.TipoFigura;
import ull.patrones.figuras.Circulo;
import ull.patrones.figuras.Cuadrado;
import ull.patrones.figuras.Figura;
import ull.patrones.figuras.Hexagono;
import ull.patrones.figuras.Rectangulo;
import ull.patrones.figuras.Rombo;
import ull.patrones.figuras.Trapecio;
import ull.patrones.figuras.Triangulo;
import ull.patrones.ui.figuras.PanelCirculo;
import ull.patrones.ui.figuras.PanelPoligono;
import ull.patrones.ui.recogerdatos.CirculoDialog;
import ull.patrones.ui.recogerdatos.CuadradoDialog;
import ull.patrones.ui.recogerdatos.HexagonoDialog;
import ull.patrones.ui.recogerdatos.RectanguloDialog;
import ull.patrones.ui.recogerdatos.RomboDialog;
import ull.patrones.ui.recogerdatos.TrapecioDialog;
import ull.patrones.ui.recogerdatos.TrianguloDialog;
/**
 * Clase encargada de mostrar la interfaz de usuario
 * @author Orlandy Ariel S�nchez A.
 *
 */
public class VentanaPrincipal extends JFrame
{
	// ATRIBUTOS
	private JPanel mPanelRadioB;
	private JPanel mPanelBotones;
	private JPanel mPanelBajo;
	private JPanel mPanelFiguraGrafica;
	private JPanel mPanelInformacion;
	private ScrollPane mScroolpanel;

	private JButton mBtnArea;
	private JButton mBtnVolumen;
	private JButton mBtnPerimetro;

	private JLabel mLBArea;
	private JLabel mLBVolumen;
	private JLabel mLBPerimetro;

	private JRadioButton mRBCirculo;
	private JRadioButton mRBTriangulo;
	private JRadioButton mRBCuadrado;
	private JRadioButton mRBRectangulo;
	private JRadioButton mRBRombo;
	private JRadioButton mRBTrapecio;
	private JRadioButton mRBHexagono;
	private ButtonGroup mBGrupoRadio;

	private Figura mFigura;

	/**
	 * Constructor por defecto
	 */
	public VentanaPrincipal()
	{
		super("Pr�ctica 2-Orlandy Ariel");
		initComponent();
	}
	private void initComponent()
	{
		confiRadio();
		configPanelRadioB();
		configGrupoRadio();
		confiPanelBotones();
		confiPanelInformacion();
		confiPanelBajo();
		confisInit();
		setVisible(true);
	}
	/**
	 * Configuraci�n inicial de la ventana
	 */
	private void confisInit()
	{
		this.setSize(600, 300);
		this.setLocationRelativeTo(null);
		this.setLayout(new BorderLayout());
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		this.setResizable(false);

		mScroolpanel = new ScrollPane();
		this.add(mScroolpanel, BorderLayout.CENTER);
		this.add(mPanelRadioB, BorderLayout.NORTH);
		this.add(mPanelBajo, BorderLayout.SOUTH);
	}
	/**
	 * Configruaci�n del panel donde se muestran los calculos
	 */
	private void confiPanelInformacion()
	{
		mLBArea = new JLabel();
		mLBArea.setVisible(true);

		mLBVolumen = new JLabel();
		mLBVolumen.setVisible(true);

		mLBPerimetro = new JLabel();
		mLBPerimetro.setVisible(true);

		mPanelInformacion = new JPanel(new FlowLayout());
		mPanelInformacion.setVisible(true);

		valoresPorDefectoLabel();
		mPanelInformacion.add(mLBArea);
		mPanelInformacion.add(mLBVolumen);
		mPanelInformacion.add(mLBPerimetro);
	}
	/**
	 * configuraci�n del panel bajo.
	 */
	private void confiPanelBajo()
	{
		mPanelBajo = new JPanel(new BorderLayout());
		mPanelBajo.setVisible(true);
		mPanelBajo.add(mPanelInformacion, BorderLayout.NORTH);
		mPanelBajo.add(mPanelBotones, BorderLayout.SOUTH);
	}

	private void confiPanelBotones()
	{
		mPanelBotones = new JPanel(new FlowLayout());

		mBtnArea = new JButton("�rea");
		mBtnArea.setVisible(true);
		mBtnArea.setEnabled(false);
		mBtnArea.addActionListener(
				new ActionListener()
				{
					@Override
					public void actionPerformed(ActionEvent e)
					{
						mostrarCalculoPerformed(mLBArea, "�rea: ", mFigura.fArea());
					}
				}
		);

		mBtnVolumen = new JButton("Volumen");
		mBtnVolumen.setVisible(true);
		mBtnVolumen.setEnabled(false);
		mBtnVolumen.addActionListener(
				new ActionListener()
				{
					@Override
					public void actionPerformed(ActionEvent e)
					{
						mostrarCalculoPerformed(mLBVolumen, "Volumen: ", mFigura.fVolumen());
					}
				}
		);

		mBtnPerimetro = new JButton("Per�metro");
		mBtnPerimetro.setVisible(true);
		mBtnPerimetro.setEnabled(false);
		mBtnPerimetro.addActionListener(
				new ActionListener()
				{
					@Override
					public void actionPerformed(ActionEvent e)
					{
						mostrarCalculoPerformed(mLBPerimetro, "Per�metro: ", mFigura.fPerimetro());
					}
				}
		);

		mPanelBotones.add(mBtnArea);
		mPanelBotones.add(mBtnVolumen);
		mPanelBotones.add(mBtnPerimetro);
	}


	private void configPanelRadioB()
	{
		mPanelRadioB = new JPanel();
		mPanelRadioB.setLayout(new FlowLayout());
		mPanelRadioB.add(mRBCirculo);
		mPanelRadioB.add(mRBCuadrado);
		mPanelRadioB.add(mRBTriangulo);
		mPanelRadioB.add(mRBRectangulo);
		mPanelRadioB.add(mRBRombo);
		mPanelRadioB.add(mRBHexagono);
		mPanelRadioB.add(mRBTrapecio);
	}

	private void configGrupoRadio()
	{
		mBGrupoRadio = new ButtonGroup();
		mBGrupoRadio.add(mRBCirculo);
		mBGrupoRadio.add(mRBCuadrado);
		mBGrupoRadio.add(mRBTriangulo);
		mBGrupoRadio.add(mRBRectangulo);
		mBGrupoRadio.add(mRBRombo);
		mBGrupoRadio.add(mRBTrapecio);
		mBGrupoRadio.add(mRBHexagono);
	}

	private void confiRadio()
	{
		mRBCirculo = new JRadioButton("C�rculo");
		mRBCirculo.setVisible(true);
		mRBCirculo.addActionListener(
				new ActionListener()
				{
					@Override
					public void actionPerformed(ActionEvent e)
					{
						seleccion();
					}
				}
		);

		mRBCuadrado = new JRadioButton("Cuadrado");
		mRBCuadrado.setVisible(true);
		mRBCuadrado.addActionListener(
				new ActionListener()
				{
					@Override
					public void actionPerformed(ActionEvent e)
					{
						seleccion();
					}
				}
		);

		mRBTriangulo = new JRadioButton("Tri�ngulo");
		mRBTriangulo.setVisible(true);
		mRBTriangulo.addActionListener(
				new ActionListener()
				{
					@Override
					public void actionPerformed(ActionEvent e)
					{
						seleccion();
					}
				}
		);

		mRBRectangulo = new JRadioButton("Rect�ngulo");
		mRBRectangulo.setVisible(true);
		mRBRectangulo.addActionListener(
				new ActionListener()
				{
					@Override
					public void actionPerformed(ActionEvent e)
					{
						seleccion();
					}
				}
		);

		mRBRombo = new JRadioButton("Rombo");
		mRBRombo.setVisible(true);
		mRBRombo.addActionListener(
				new ActionListener()
				{
					@Override
					public void actionPerformed(ActionEvent e)
					{
						seleccion();
					}
				}
		);

		mRBTrapecio = new JRadioButton("Trapecio");
		mRBTrapecio.setVisible(true);
		mRBTrapecio.addActionListener(
				new ActionListener()
				{
					@Override
					public void actionPerformed(ActionEvent e)
					{
						seleccion();
					}
				}
		);

		mRBHexagono = new JRadioButton("Hexagono");
		mRBHexagono.setVisible(true);
		mRBHexagono.addActionListener(
				new ActionListener()
				{
					@Override
					public void actionPerformed(ActionEvent e)
					{
						seleccion();
					}
				}
		);
	}

	private void seleccion()
	{
		if (mRBCirculo.isSelected())
		{
			recogerDatos(TipoFigura.CIRCULO);
			mPanelFiguraGrafica = new PanelCirculo();
		} else if (mRBCuadrado.isSelected())
		{
			recogerDatos(TipoFigura.CUADRADO);
			mPanelFiguraGrafica = new PanelPoligono(TipoFigura.CUADRADO);
		} else if (mRBTriangulo.isSelected())
		{
			recogerDatos(TipoFigura.TRIANGULO);
			mPanelFiguraGrafica = new PanelPoligono(TipoFigura.TRIANGULO);
		} else if (mRBRectangulo.isSelected())
		{
			recogerDatos(TipoFigura.RECTANGULO);
			mPanelFiguraGrafica = new PanelPoligono(TipoFigura.RECTANGULO);
		} else if (mRBRombo.isSelected())
		{
			recogerDatos(TipoFigura.ROMBO);
			mPanelFiguraGrafica = new PanelPoligono(TipoFigura.ROMBO);
		} else if (mRBTrapecio.isSelected())
		{
			recogerDatos(TipoFigura.TRAPECIO);
			mPanelFiguraGrafica = new PanelPoligono(TipoFigura.TRAPECIO);
		} else if (mRBHexagono.isSelected())
		{
			recogerDatos(TipoFigura.HEXAGONO);
			mPanelFiguraGrafica = new PanelPoligono(TipoFigura.HEXAGONO);
		}
		valoresPorDefectoLabel();
		mScroolpanel.add(mPanelFiguraGrafica);
	}

	private void habilitarDialogo(JDialog aDialog)
	{
		aDialog.setVisible(true);
		mBtnArea.setEnabled(true);
		mBtnVolumen.setEnabled(true);
		mBtnPerimetro.setEnabled(true);
	}
	/**
	 * m�todo, que dependiendo de la figura, recoge los datos que el usuario introduce
	 * @param aFiguras
	 */
	private void recogerDatos(TipoFigura aFiguras)
	{
		switch (aFiguras)
		{
		case CIRCULO:
			CirculoDialog tCirculo = new CirculoDialog(this, "Datos del Circulo", true);
			habilitarDialogo(tCirculo);
			mFigura = new Circulo(tCirculo.getMRadio());
			break;
		case TRIANGULO:
			TrianguloDialog tTriangulo = new TrianguloDialog(this, "Datos del Tri�ngulo", true);
			habilitarDialogo(tTriangulo);
			mFigura = new Triangulo(tTriangulo.getMBase(), tTriangulo.getMAltura());
			break;
		case CUADRADO:
			CuadradoDialog tCuadrado = new CuadradoDialog(this, "Datos del Cuadrado", true);
			habilitarDialogo(tCuadrado);
			mFigura = new Cuadrado(tCuadrado.getMLado());
			break;
		case RECTANGULO:
			RectanguloDialog tRectangulo = new RectanguloDialog(this, "Datos del Rect�ngulo", true);
			habilitarDialogo(tRectangulo);
			mFigura = new Rectangulo(
					tRectangulo.getMLongitud(), tRectangulo.getMAltura(), tRectangulo.getMProfundidad()
			);
			break;
		case TRAPECIO:
			TrapecioDialog tTrapecio = new TrapecioDialog(this, "Datos del Trapecio", true);
			habilitarDialogo(tTrapecio);
			mFigura = new Trapecio(
					tTrapecio.getMBaseMenor(), tTrapecio.getMBaseMayor(), tTrapecio.getMAltura(), tTrapecio.getMLado()
			);
			break;
		case HEXAGONO:
			HexagonoDialog tHexagono = new HexagonoDialog(this, "Datos del Hexagono", true);
			habilitarDialogo(tHexagono);
			mFigura = new Hexagono(tHexagono.getMLongitudLado(), tHexagono.getMAltura());
			break;
		case ROMBO:
			RomboDialog tRombo = new RomboDialog(this, "Datos del Rombo", true);
			habilitarDialogo(tRombo);
			mFigura = new Rombo(tRombo.getMDiagonalMayor(), tRombo.getMDiagonalMenor(), tRombo.getMLado());
			break;
		default:
			break;
		}
	}
	/**
	 * M�todo para visualizar los datos calculados con cierto formato
	 * @param aLabel
	 * @param aTexto
	 * @param aDato
	 */
	private void mostrarCalculoPerformed(JLabel aLabel, String aTexto, Double aDato)
	{
		DecimalFormat formatoDosDigitos = new DecimalFormat("##.##");

		aLabel.setText(aTexto + formatoDosDigitos.format(aDato));
	}
	/**
	 * M�todo para resetear los valores de los c�lculos
	 */
	private void valoresPorDefectoLabel()
	{
		mLBArea.setText("�rea: -- ");
		mLBVolumen.setText("Volumen: -- ");
		mLBPerimetro.setText("Per�metro: -- ");
	}
}
