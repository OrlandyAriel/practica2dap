package ull.patrones.ui.recogerdatos;

import java.awt.BorderLayout;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
/**
 * Clase que recoge los datos, en un formulario, para representar realizar los c�lculos de un Trapecio
 * @author Orlandy Ariel S�nchez A.
 *
 */
public class TrapecioDialog extends JDialog
{
	private Double mBaseMenor;
	private Double mBaseMayor;
	private Double mLado;
	private Double mAltura;

	private JTextField mTxtBaseMayor;
	private JTextField mTxtBaseMenor;
	private JTextField mTxtLado;
	private JTextField mTxtAltura;

	private JLabel mLbBaseMayor;
	private JLabel mLbBaseMenor;
	private JLabel mLbLado;
	private JLabel mLbAltura;

	private JButton mBtnEnviar;

	private JPanel mPanelDerecha;
	private JPanel mPanelIquierda;
	/**
	 * 
	 * @param aFrame
	 * @param aTitulo
	 * @param aModal
	 */
	public TrapecioDialog(JFrame aFrame, String aTitulo, boolean aModal)
	{
		super(aFrame, aTitulo, aModal);
		initComponent();
	}

	private void initComponent()
	{
		this.setLayout(new BorderLayout());
		this.setResizable(false);
		this.setSize(200, 140);
		this.setLocationRelativeTo(null);

		mBtnEnviar = new JButton("Enviar");
		mBtnEnviar.setVisible(true);
		mBtnEnviar.addActionListener(
				new ActionListener()
				{
					@Override
					public void actionPerformed(ActionEvent arg0)
					{
						enviarActinonPerformed();
					}

				}
		);
		confiPanelDerecho();
		confiPanelIzquierdo();
		this.add(mPanelIquierda, BorderLayout.WEST);
		this.add(mPanelDerecha, BorderLayout.EAST);
		this.add(mBtnEnviar, BorderLayout.SOUTH);
	}

	private void enviarActinonPerformed()
	{
		mBaseMayor = Double.parseDouble(mTxtBaseMayor.getText());
		mBaseMenor = Double.parseDouble(mTxtBaseMenor.getText());
		mAltura = Double.parseDouble(mTxtAltura.getText());
		mLado = Double.parseDouble(mTxtLado.getText());
		dispose();
	}

	private void confiPanelIzquierdo()
	{
		mPanelIquierda = new JPanel();
		mPanelIquierda.setLayout(new BoxLayout(mPanelIquierda, BoxLayout.Y_AXIS));

		mLbBaseMayor = new JLabel("Base mayor");
		bordeLabel(mLbBaseMayor);

		mLbBaseMenor = new JLabel("Base menor");
		bordeLabel(mLbBaseMenor);

		mLbLado = new JLabel("Lado");
		bordeLabel(mLbLado);

		mLbAltura = new JLabel("Altura");
		bordeLabel(mLbAltura);

		mPanelIquierda.add(mLbBaseMayor);
		mPanelIquierda.add(mLbBaseMenor);
		mPanelIquierda.add(mLbLado);
		mPanelIquierda.add(mLbAltura);
	}

	private void confiPanelDerecho()
	{
		mPanelDerecha = new JPanel();
		mPanelDerecha.setLayout(new BoxLayout(mPanelDerecha, BoxLayout.Y_AXIS));

		mTxtBaseMayor = new JTextField();
		tamTxtFiel(mTxtBaseMayor);

		mTxtBaseMenor = new JTextField();
		tamTxtFiel(mTxtBaseMayor);

		mTxtLado = new JTextField();
		tamTxtFiel(mTxtLado);

		mTxtAltura = new JTextField();
		tamTxtFiel(mTxtAltura);

		mPanelDerecha.add(mTxtBaseMayor);
		mPanelDerecha.add(mTxtBaseMenor);
		mPanelDerecha.add(mTxtLado);
		mPanelDerecha.add(mTxtAltura);

	}

	private void bordeLabel(JLabel aLb)
	{
		aLb.setVisible(true);
		aLb.setBounds(new Rectangle(5, 15, 220, 21));// mirar
	}

	private void tamTxtFiel(JTextField aTxt)
	{
		aTxt.setVisible(true);
		aTxt.setColumns(10);
	}

	public Double getMBaseMenor()
	{
		return mBaseMenor;
	}

	public void setMBaseMenor(Double mBaseMenor)
	{
		this.mBaseMenor = mBaseMenor;
	}

	public Double getMBaseMayor()
	{
		return mBaseMayor;
	}

	public void setMBaseMayor(Double mBaseMayor)
	{
		this.mBaseMayor = mBaseMayor;
	}

	public Double getMLado()
	{
		return mLado;
	}

	public void setMLado(Double mLado)
	{
		this.mLado = mLado;
	}

	public Double getMAltura()
	{
		return mAltura;
	}

	public void setMAltura(Double mAltura)
	{
		this.mAltura = mAltura;
	}

}
