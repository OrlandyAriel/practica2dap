package ull.patrones.ui.recogerdatos;

import java.awt.BorderLayout;
import java.awt.Frame;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JTextField;

/**
 * Clase que recoge los datos, en un formulario, para representar realizar los
 * c�lculos de un Circulo
 * 
 * @author Orlandy Ariel S�nchez A.
 */
public class CirculoDialog extends JDialog
{
	// ATRIBUTOS
	private JLabel mLabRadio;
	private JTextField mTxtRadio;
	private JButton mBtnEnviar;
	private String mRadio;

	// CONSTRUCTOR/ES
	public CirculoDialog(Frame aVentana, String aTitulo, boolean aModal)
	{
		super(aVentana, aTitulo, aModal);
		initComponent();
	}

	// M�TODOS Y FUNCIONES
	private void initComponent()
	{
		mLabRadio = new JLabel("Radio");
		mLabRadio.setVisible(true);

		mTxtRadio = new JTextField(5);

		mTxtRadio.setVisible(true);

		mBtnEnviar = new JButton("Enviar");
		mBtnEnviar.setVisible(true);
		mBtnEnviar.addActionListener(
				new ActionListener()
				{
					@Override
					public void actionPerformed(ActionEvent e)
					{
						mRadio = mTxtRadio.getText();
						dispose();
					}
				}
		);
		this.setLayout(new BorderLayout());
		this.setSize(200, 90);
		this.setLocationRelativeTo(null);
		this.add(mLabRadio, BorderLayout.WEST);
		this.add(mTxtRadio, BorderLayout.EAST);
		this.add(mBtnEnviar, BorderLayout.SOUTH);

	}

	public Double getMRadio()
	{
		return Double.parseDouble(mRadio);
	}
}
