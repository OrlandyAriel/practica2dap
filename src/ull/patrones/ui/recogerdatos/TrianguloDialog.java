package ull.patrones.ui.recogerdatos;

import java.awt.BorderLayout;
import java.awt.Frame;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;

/** 
 * Clase que recoge los datos, en un formulario, para representar realizar los c�lculos de un Tri�ngulo
 * @author Orlandy Ariel S�nchez A. 
 */
public class TrianguloDialog extends JDialog
{
	// ATRIBUTOS
	private JLabel mLbBase;
	private JLabel mLbAltura;

	private JTextField mTxtBase;
	private JTextField mTxtAltura;

	private JButton mBtnEnviar;

	private String mBase;
	private String mAltura;

	// CONSTRUCTOR/ES
	public TrianguloDialog(Frame aVentana, String aTitulo, boolean aModal)
	{
		super(aVentana, aTitulo, aModal);
		initComponent();
	}

	// M�TODOS Y FUNCIONES
	private void initComponent()
	{

		JPanel panelLabel = new JPanel();
		panelLabel.setLayout(new BoxLayout(panelLabel, BoxLayout.Y_AXIS));

		JPanel panelText = new JPanel();
		panelText.setLayout(new BoxLayout(panelText, BoxLayout.Y_AXIS));

		mBtnEnviar = new JButton("Enviar");
		mBtnEnviar.setVisible(true);

		mLbBase = new JLabel("Base:");
		mLbBase.setVisible(true);

		mLbAltura = new JLabel("Altura:");
		mLbAltura.setVisible(true);

		mTxtBase = new JTextField();
		mTxtBase.setVisible(true);
		mTxtBase.setColumns(10);

		mTxtAltura = new JTextField();
		mTxtAltura.setColumns(10);
		mTxtAltura.setVisible(true);

		mBtnEnviar.addActionListener(
				new ActionListener()
				{
					@Override
					public void actionPerformed(ActionEvent e)
					{
						if (!mTxtAltura.getText().isEmpty() && !mTxtBase.getText().isEmpty())
						{
							mAltura = mTxtAltura.getText();
							mBase = mTxtBase.getText();
							dispose();
						} else
						{
							JOptionPane.showMessageDialog(
									null, "Error, no puedes dejar campos en blanco.", "Mensaje de Error",
									JOptionPane.ERROR_MESSAGE
							);

						}
					}
				}
		);
		panelText.add(mTxtBase);
		panelText.add(mTxtAltura);

		panelLabel.add(mLbBase);
		panelLabel.add(mLbAltura);

		this.setLayout(new BorderLayout());
		this.setResizable(false);
		this.setSize(200, 100);
		this.setLocationRelativeTo(null);
		this.add(panelLabel, BorderLayout.WEST);
		this.add(panelText, BorderLayout.EAST);
		this.add(mBtnEnviar, BorderLayout.SOUTH);
	}

	public Double getMBase()
	{
		return Double.parseDouble(mBase);
	}

	public Double getMAltura()
	{
		return Double.parseDouble(mAltura);
	}
}
