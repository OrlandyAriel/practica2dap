package ull.patrones.ui.recogerdatos;

import java.awt.BorderLayout;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
/**
 * Clase que recoge los datos, en un formulario, para representar realizar los c�lculos de un
 * @author Orlandy Ariel S�nchez A.
 *
 */
public class RomboDialog extends JDialog
{	//ATRIBUTOS
	private Double mDiagonalMayor;
	private Double mDiagonalMenor;
	private Double mLado;

	private JLabel mLbDiagonalMayor;
	private JLabel mLbDiagonalMenor;
	private JLabel mLbLado;

	private JTextField mTxtDiagonalMayor;
	private JTextField mTxtDiagonalMenor;
	private JTextField mTxtLado;

	private JButton mBtnEnviar;

	private JPanel mPanelIzuqierdo;
	private JPanel mPanelDerecho;
	/**
	 * 
	 * @param aFrame
	 * @param aTitulo
	 * @param aModal
	 */
	public RomboDialog(JFrame aFrame, String aTitulo, boolean aModal)
	{
		super(aFrame, aTitulo, aModal);
		initComponent();
	}

	private void initComponent()
	{

		this.setLayout(new BorderLayout());
		this.setResizable(false);
		this.setSize(250, 115);
		this.setLocationRelativeTo(null);

		mBtnEnviar = new JButton("Enviar");
		mBtnEnviar.setVisible(true);
		mBtnEnviar.addActionListener(
				new ActionListener()
				{
					@Override
					public void actionPerformed(ActionEvent arg0)
					{
						enviarActinonPerformed();
					}
				}
		);
		configPanelIquierdo();
		confiPanelDerecho();
		this.add(mPanelIzuqierdo, BorderLayout.WEST);
		this.add(mPanelDerecho, BorderLayout.EAST);
		this.add(mBtnEnviar, BorderLayout.SOUTH);
	}

	private void enviarActinonPerformed()
	{
		mDiagonalMayor = Double.parseDouble(mTxtDiagonalMayor.getText());
		mDiagonalMenor = Double.parseDouble(mTxtDiagonalMenor.getText());
		mLado = Double.parseDouble(mTxtLado.getText());
		dispose();
	}

	private void confiPanelDerecho()
	{
		mPanelDerecho = new JPanel();
		mPanelDerecho.setLayout(new BoxLayout(mPanelDerecho, BoxLayout.Y_AXIS));

		mTxtDiagonalMayor = new JTextField();
		tamTxtFiel(mTxtDiagonalMayor);

		mTxtDiagonalMenor = new JTextField();
		tamTxtFiel(mTxtDiagonalMenor);

		mTxtLado = new JTextField();
		tamTxtFiel(mTxtLado);

		mPanelDerecho.add(mTxtDiagonalMayor);
		mPanelDerecho.add(mTxtDiagonalMenor);
		mPanelDerecho.add(mTxtLado);
	}

	private void configPanelIquierdo()
	{
		mPanelIzuqierdo = new JPanel();
		mPanelIzuqierdo.setLayout(new BoxLayout(mPanelIzuqierdo, BoxLayout.Y_AXIS));

		mLbDiagonalMayor = new JLabel("Diagonal mayor");
		bordeLabel(mLbDiagonalMayor);

		mLbDiagonalMenor = new JLabel("Diagonal Menor");
		bordeLabel(mLbDiagonalMenor);

		mLbLado = new JLabel("Lado");
		bordeLabel(mLbLado);

		mPanelIzuqierdo.add(mLbDiagonalMayor);
		mPanelIzuqierdo.add(mLbDiagonalMenor);
		mPanelIzuqierdo.add(mLbLado);
	}

	private void bordeLabel(JLabel aLb)
	{
		aLb.setVisible(true);
		aLb.setBounds(new Rectangle(5, 15, 220, 21));// mirar
	}

	private void tamTxtFiel(JTextField aTxt)
	{
		aTxt.setVisible(true);
		aTxt.setColumns(10);
	}

	public Double getMDiagonalMayor()
	{
		return mDiagonalMayor;
	}

	public void setMDiagonalMayor(Double mDiagonalMayor)
	{
		this.mDiagonalMayor = mDiagonalMayor;
	}

	public Double getMDiagonalMenor()
	{
		return mDiagonalMenor;
	}

	public void setMDiagonalMenor(Double mDiagonalMenor)
	{
		this.mDiagonalMenor = mDiagonalMenor;
	}

	public Double getMLado()
	{
		return mLado;
	}

	public void setMLado(Double mLado)
	{
		this.mLado = mLado;
	}
}
