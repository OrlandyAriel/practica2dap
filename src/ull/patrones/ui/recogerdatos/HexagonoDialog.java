package ull.patrones.ui.recogerdatos;

import java.awt.BorderLayout;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
/**
 * Clase que recoge los datos, en un formulario, para representar realizar los c�lculos de un Hexagono
 * @author Orlandy Ariel S�nchez A.
 *
 */
public class HexagonoDialog extends JDialog
{ //ATRIBUTOS
	private Double mLongitudLado;
	private Double mAltura;

	private JTextField mTxtLongitudLado;
	private JTextField mTxtAltura;

	private JLabel mLbLongitudLado;
	private JLabel mLbAltura;

	private JPanel mPanelDerecho;
	private JPanel mPanelIzuqierdo;

	private JButton mBtnEnviar;
	/**
	 * 
	 * @param aFrame
	 * @param aTitulo
	 * @param aModad
	 */
	public HexagonoDialog(JFrame aFrame, String aTitulo, boolean aModad)
	{
		super(aFrame, aTitulo, aModad);
		initComponent();
	}

	private void initComponent()
	{
		this.setLayout(new BorderLayout());
		this.setResizable(false);
		this.setSize(200, 100);
		this.setLocationRelativeTo(null);

		mBtnEnviar = new JButton("Enviar");
		mBtnEnviar.setVisible(true);
		mBtnEnviar.addActionListener(
				new ActionListener()
				{
					@Override
					public void actionPerformed(ActionEvent arg0)
					{
						enviarActinonPerformed();
					}

				}
		);
		configPanelIquierdo();
		confiPanelDerecho();
		this.add(mPanelIzuqierdo, BorderLayout.WEST);
		this.add(mPanelDerecho, BorderLayout.EAST);
		this.add(mBtnEnviar, BorderLayout.SOUTH);
	}

	private void enviarActinonPerformed()
	{
		mLongitudLado = Double.parseDouble(mTxtLongitudLado.getText());
		mAltura = Double.parseDouble(mTxtAltura.getText());
		dispose();
	}

	private void configPanelIquierdo()
	{
		mPanelIzuqierdo = new JPanel();
		mPanelIzuqierdo.setLayout(new BoxLayout(mPanelIzuqierdo, BoxLayout.Y_AXIS));

		mLbAltura = new JLabel("Altura");
		bordeLabel(mLbAltura);

		mLbLongitudLado = new JLabel("Longitud");
		bordeLabel(mLbLongitudLado);

		mPanelIzuqierdo.add(mLbAltura);
		mPanelIzuqierdo.add(mLbLongitudLado);
	}

	private void confiPanelDerecho()
	{
		mPanelDerecho = new JPanel();
		mPanelDerecho.setLayout(new BoxLayout(mPanelDerecho, BoxLayout.Y_AXIS));
		mTxtAltura = new JTextField();
		tamTxtFiel(mTxtAltura);

		mTxtLongitudLado = new JTextField();
		tamTxtFiel(mTxtLongitudLado);

		mPanelDerecho.add(mTxtAltura);
		mPanelDerecho.add(mTxtLongitudLado);
	}

	private void bordeLabel(JLabel aLb)
	{
		aLb.setVisible(true);
		aLb.setBounds(new Rectangle(5, 15, 220, 21));// mirar
	}

	private void tamTxtFiel(JTextField aTxt)
	{
		aTxt.setVisible(true);
		aTxt.setColumns(10);
	}

	public Double getMLongitudLado()
	{
		return mLongitudLado;
	}

	public void setMLongitudLado(Double mLongitudLado)
	{
		this.mLongitudLado = mLongitudLado;
	}

	public Double getMAltura()
	{
		return mAltura;
	}

	public void setMAltura(Double mAltura)
	{
		this.mAltura = mAltura;
	}

}
