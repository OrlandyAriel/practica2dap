package ull.patrones.ui.recogerdatos;

import java.awt.BorderLayout;
import java.awt.Frame;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JTextField;

/**
 * * Clase que recoge los datos, en un formulario, para representar realizar los
 * c�lculos de un Cuadrado
 * 
 * @author Orlandy Ariel S�nchez A.
 */
public class CuadradoDialog extends JDialog
{
	// ATRIBUTOS
	private JLabel mLBLado;
	private JTextField mTxtLado;

	private JButton mBtnEnviar;
	private String mLado;

	// CONSTRUCTOR/ES
	public CuadradoDialog(Frame aVentana, String aTitulo, boolean aModal)
	{
		super(aVentana, aTitulo, aModal);
		initComponent();
	}

	// M�TODOS Y FUNCIONES
	private void initComponent()
	{
		mLBLado = new JLabel("Lado:");
		mLBLado.setVisible(true);

		mTxtLado = new JTextField(5);
		mTxtLado.setVisible(true);

		mBtnEnviar = new JButton("Enviar");
		mBtnEnviar.setVisible(true);
		mBtnEnviar.addActionListener(
				new ActionListener()
				{
					@Override
					public void actionPerformed(ActionEvent e)
					{

						mLado = mTxtLado.getText();
						dispose();

					}

				}
		);
		this.setLayout(new BorderLayout());
		this.setSize(200, 90);
		this.setLocationRelativeTo(null);
		this.add(mLBLado, BorderLayout.WEST);
		this.add(mTxtLado, BorderLayout.EAST);
		this.add(mBtnEnviar, BorderLayout.SOUTH);
	}

	public Double getMLado()
	{
		return Double.parseDouble(mLado.toString());
	}
}
