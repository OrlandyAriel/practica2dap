package ull.patrones.ui.recogerdatos;

import java.awt.BorderLayout;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
/**
 * Clase que recoge los datos, en un formulario, para representar realizar los c�lculos de un Rect�ngulo
 * @author Orlandy Ariel S�nchez A.
 *
 */
public class RectanguloDialog extends JDialog
{
	//ATRIBUTOS
	private Double mLongitud;
	private Double mAltura;
	private Double mProfundidad;

	private JLabel mLbLongitud;
	private JLabel mLbAltura;
	private JLabel mLbProfundidad;

	private JTextField mTxtLongitud;
	private JTextField mTxtAltura;
	private JTextField mTxtProfundidad;

	private JButton mBtnEnviar;

	private JPanel mPanelDerecho;
	private JPanel mPanelIzquierdo;
	/**
	 * 
	 * @param aFrame
	 * @param aTitulo
	 * @param aModal
	 */
	public RectanguloDialog(JFrame aFrame, String aTitulo, boolean aModal)
	{
		super(aFrame, aTitulo, aModal);
		initComponent();
	}

	private void initComponent()
	{
		this.setLayout(new BorderLayout());
		this.setResizable(false);
		this.setSize(250, 115);
		this.setLocationRelativeTo(null);

		mBtnEnviar = new JButton("Enviar");
		mBtnEnviar.setVisible(true);
		mBtnEnviar.addActionListener(
				new ActionListener()
				{
					@Override
					public void actionPerformed(ActionEvent arg0)
					{
						enviarActinonPerformed();
					}
				}
		);

		configPanelIquierdo();
		confiPanelDerecho();
		this.add(mPanelIzquierdo, BorderLayout.WEST);
		this.add(mPanelDerecho, BorderLayout.EAST);
		this.add(mBtnEnviar, BorderLayout.SOUTH);
	}

	private void enviarActinonPerformed()
	{
		mLongitud = Double.parseDouble(mTxtAltura.getText());
		mAltura = Double.parseDouble(mTxtAltura.getText());
		mProfundidad = Double.parseDouble(mTxtProfundidad.getText());
		dispose();
	}

	private void confiPanelDerecho()
	{
		mPanelDerecho = new JPanel();
		mPanelDerecho.setLayout(new BoxLayout(mPanelDerecho, BoxLayout.Y_AXIS));

		mTxtAltura = new JTextField();
		tamTxtFiel(mTxtAltura);

		mTxtLongitud = new JTextField();
		tamTxtFiel(mTxtLongitud);

		mTxtProfundidad = new JTextField();
		tamTxtFiel(mTxtProfundidad);

		mPanelDerecho.add(mTxtAltura);
		mPanelDerecho.add(mTxtLongitud);
		mPanelDerecho.add(mTxtProfundidad);
	}

	private void configPanelIquierdo()
	{
		mPanelIzquierdo = new JPanel();
		mPanelIzquierdo.setLayout(new BoxLayout(mPanelIzquierdo, BoxLayout.Y_AXIS));

		mLbAltura = new JLabel("Altura");
		bordeLabel(mLbAltura);

		mLbLongitud = new JLabel("Longitud");
		bordeLabel(mLbLongitud);

		mLbProfundidad = new JLabel("Profundidad");
		bordeLabel(mLbProfundidad);

		mPanelIzquierdo.add(mLbAltura);
		mPanelIzquierdo.add(mLbLongitud);
		mPanelIzquierdo.add(mLbProfundidad);
	}

	private void bordeLabel(JLabel aLb)
	{
		aLb.setVisible(true);
		aLb.setBounds(new Rectangle(5, 15, 220, 21));// mirar
	}

	private void tamTxtFiel(JTextField aTxt)
	{
		aTxt.setVisible(true);
		aTxt.setColumns(10);
	}

	public Double getMLongitud()
	{
		return mLongitud;
	}

	public void setMLongitud(Double mLongitud)
	{
		this.mLongitud = mLongitud;
	}

	public Double getMAltura()
	{
		return mAltura;
	}

	public void setMAltura(Double mAltura)
	{
		this.mAltura = mAltura;
	}

	public Double getMProfundidad()
	{
		return mProfundidad;
	}

	public void setMProfundidad(Double mProfundidad)
	{
		this.mProfundidad = mProfundidad;
	}
}
